fun main(){
    val user = setUser("Syah Hakam", 25)
    printUser(user)
}

fun setUser(name: String, age: Int) = "Your name is $name and your age is $age"

fun printUser(name: String){
    println("$name")
}