abstract class Fruits(
    var name: String,
    var weight: Double,
    var isApple: Boolean
) {
    fun flavor(){
        println("$name apple!")
    }

    fun shape(){
        println("$name rounded!")
    }
}

fun main(){
    println("Abstract Class")
}