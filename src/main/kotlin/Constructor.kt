class Animal {
    val name: String = "Dog"
    val weight: Double = 5.0
    val age: Int = 4
    val isMammal: Boolean = true
}

fun main() {
    val dog = Animal()
    println(
        "Name: ${dog.name}, Weight: ${dog.weight}, Age: ${dog.age}, Mammal: ${dog.isMammal}"
    )
}